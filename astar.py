import heapq as hq
import numpy as np


class Node:
    def __init__(self, parent=None, position=None):
        self.parent = parent
        self.position = position

        self.g = 0   # Cost of getting to this node
        self.h = 0   # Estimated cost to goal (from this node)
        self.f = 0   # Estimated total cost of a solution path going through this node

    def __eq__(self, other):
        return self.position == other.position

    def __lt__(self, other):   # Comparison class so heapq can retrieve the correct object with smallest f value
        return self.f < other.f


def ASTARE(maze, start, end):
    """Returns a list of tuples as a path from the given start to the given end in the given maze"""
    start = tuple(start)  # Convert input points to tuples
    end = tuple(end)      #

    # Create start and end node
    start_node = Node(None, start)
    start_node.g = start_node.h = start_node.f = 0
    end_node = Node(None, end)
    end_node.g = end_node.h = end_node.f = 0

    # Initialize both open and closed list
    open_list = []
    closed_list = []

    # Add the start node
    hq.heappush(open_list, start_node)

    # Loop until you find the end
    while len(open_list) > 0:
        current_node = hq.heappop(open_list)
        hq.heappush(closed_list,current_node)

        # Found the goal
        if current_node == end_node:
            print("FOUND END NODE")
            path = []
            current = current_node
            while current is not None:
                path.append(current.position)  # Append the node higher up in the tree
                current = current.parent       # Get the next parent
            return path[::-1], closed_list     # Return reversed path and the visited nodes for visualization

        # Generate children
        children = []
        for new_position in [(0, -1), (0, 1), (-1, 0), (1, 0)]: # Squares in proximity

            # Get node position
            node_position = (current_node.position[0] + new_position[0], current_node.position[1] + new_position[1])

            # Make sure within range
            if node_position[0] > (len(maze) - 1) or node_position[0] < 0 or node_position[1] > (len(maze[len(maze)-1]) -1) or node_position[1] < 0:
                continue

            # Make sure walkable terrain
            if maze[node_position[0]][node_position[1]] == -1:  #If it hits (-1) border
                continue

            # Create new node
            new_node = Node(current_node, node_position)

            # Append
            children.append(new_node)

        # Loop through children
        for child in children:
            #  does the child exist in closed list? Stop checking it
            if child in closed_list:
                continue

            child.g = current_node.g + maze[child.position[0]][child.position[1]]  # Cost of getting to this node
            child.h = np.abs(child.position[0] - end_node.position[0]) + np.abs(child.position[1] - end_node.position[1])   # Manhattan distance heuristic
            child.f = child.g + child.h   # Total estimated distance from start to goal through this node

            # Does the child exist in open list and has a bigger cost than any of the nodes in open list? Dont add
            if child in open_list and any(child.g > x.g for x in open_list):
                continue

            # Add the child to the open list
            hq.heappush(open_list,child)